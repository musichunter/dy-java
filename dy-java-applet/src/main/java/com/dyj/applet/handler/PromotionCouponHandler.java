package com.dyj.applet.handler;

import com.dyj.applet.domain.query.BatchConsumeCouponQuery;
import com.dyj.applet.domain.query.BatchRollbackConsumeCouponQuery;
import com.dyj.applet.domain.query.BindUserToSidebarActivityQuery;
import com.dyj.applet.domain.query.QueryCouponReceiveInfoQuery;
import com.dyj.applet.domain.vo.ConsumeCouponIdListVo;
import com.dyj.applet.domain.vo.QueryCouponReceiveInfoVo;
import com.dyj.common.config.AgentConfiguration;
import com.dyj.common.domain.DySimpleResult;

/**
 * 小程序券
 */
public class PromotionCouponHandler extends AbstractAppletHandler {
    public PromotionCouponHandler(AgentConfiguration agentConfiguration) {
        super(agentConfiguration);
    }

    /**
     * 查询用户可用券信息
     * @param body 查询用户可用券信息请求值
     * @return
     */
    public DySimpleResult<QueryCouponReceiveInfoVo> queryCouponReceiveInfo(QueryCouponReceiveInfoQuery body){
        userInfoQuery(body);
        return getPromotionCouponClient().queryCouponReceiveInfo(body);
    }

    /**
     * 用户撤销核销券
     * @param body 用户撤销核销券请求值
     * @return
     */
    public DySimpleResult<ConsumeCouponIdListVo> batchRollbackConsumeCoupon(BatchRollbackConsumeCouponQuery body){
        userInfoQuery(body);
        return getPromotionCouponClient().batchRollbackConsumeCoupon(body);
    }


    /**
     * 复访营销活动实时圈选用户
     * @param body 复访营销活动实时圈选用户请求值
     * @return
     */
    public DySimpleResult<?> bindUserToSidebarActivity(BindUserToSidebarActivityQuery body){
        userInfoQuery(body);
        return getPromotionCouponClient().bindUserToSidebarActivity(body);
    }


    /**
     * 用户核销券
     * @param body 用户核销券请求值
     * @return
     */
    public DySimpleResult<ConsumeCouponIdListVo> batchConsumeCoupon(BatchConsumeCouponQuery body){
        userInfoQuery(body);
        return getPromotionCouponClient().batchConsumeCoupon(body);
    }
}
