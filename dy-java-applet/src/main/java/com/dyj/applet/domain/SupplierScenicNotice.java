package com.dyj.applet.domain;

import java.util.List;

/**
 * 景区须知
 *
 * @author danmo
 * @date 2024-04-28 14:19
 **/
public class SupplierScenicNotice {

    /**
     * 玩法介绍(不超过200个汉字)
     */
    private List<ScenicNoticeIntro> play_intro;
    /**
     * 优待政策(不超过200个汉字)
     */
    private List<ScenicNoticePreferentialPolicy> preferential_policy;
    /**
     * 优待政策-扩展区(不超过1000个汉字)
     */
    private List<ScenicNoticeIntro> preferential_policy_ext;
    /**
     * 服务设施列表
     */
    private List<ScenicNoticeFacility> facility;
    /**
     * 开放时间
     */
    private String open_time;
    /**
     * 开放时间-扩展区(不超过200个汉字)
     */
    private List<ScenicNoticeIntro> open_time_ext;
    /**
     * 宠物携带(1:可携带宠物，2:不可携带宠物)
     */
    private Integer pet;
    /**
     * 景点介绍(不超过2000个汉字)
     */
    private List<ScenicNoticeIntro> scenic_intro;
    /**
     * 交通(不超过200个汉字)
     */
    private List<ScenicNoticeIntro> traffic;

    public static SupplierScenicNoticeBuilder builder() {
        return new SupplierScenicNoticeBuilder();
    }

    public static class SupplierScenicNoticeBuilder {
        private List<ScenicNoticeIntro> playIntro;
        private List<ScenicNoticePreferentialPolicy> preferentialPolicy;
        private List<ScenicNoticeIntro> preferentialPolicyExt;
        private String openTime;
        private List<ScenicNoticeIntro> openTimeExt;
        private Integer pet;
        private List<ScenicNoticeIntro> scenicIntro;
        private List<ScenicNoticeIntro> traffic;

        public SupplierScenicNoticeBuilder playIntro(List<ScenicNoticeIntro> playIntro) {
            this.playIntro = playIntro;
            return this;
        }

        public SupplierScenicNoticeBuilder preferentialPolicy(List<ScenicNoticePreferentialPolicy> preferentialPolicy) {
            this.preferentialPolicy = preferentialPolicy;
            return this;
        }

        public SupplierScenicNoticeBuilder preferentialPolicyExt(List<ScenicNoticeIntro> preferentialPolicyExt) {
            this.preferentialPolicyExt = preferentialPolicyExt;
            return this;
        }

        public SupplierScenicNoticeBuilder openTime(String openTime) {
            this.openTime = openTime;
            return this;
        }

        public SupplierScenicNoticeBuilder openTimeExt(List<ScenicNoticeIntro> openTimeExt) {
            this.openTimeExt = openTimeExt;
            return this;
        }

        public SupplierScenicNoticeBuilder pet(Integer pet) {
            this.pet = pet;
            return this;
        }

        public SupplierScenicNoticeBuilder scenicIntro(List<ScenicNoticeIntro> scenicIntro) {
            this.scenicIntro = scenicIntro;
            return this;
        }

        public SupplierScenicNoticeBuilder traffic(List<ScenicNoticeIntro> traffic) {
            this.traffic = traffic;
            return this;
        }

        public SupplierScenicNotice build() {
            SupplierScenicNotice supplierScenicNotice = new SupplierScenicNotice();
            supplierScenicNotice.setOpen_time(openTime);
            supplierScenicNotice.setOpen_time_ext(openTimeExt);
            supplierScenicNotice.setPet(pet);
            supplierScenicNotice.setPreferential_policy(preferentialPolicy);
            supplierScenicNotice.setPreferential_policy_ext(preferentialPolicyExt);
            supplierScenicNotice.setPlay_intro(playIntro);
            supplierScenicNotice.setScenic_intro(scenicIntro);
            supplierScenicNotice.setTraffic(traffic);
            return supplierScenicNotice;
        }
    }

    public List<ScenicNoticeIntro> getPlay_intro() {
        return play_intro;
    }

    public void setPlay_intro(List<ScenicNoticeIntro> play_intro) {
        this.play_intro = play_intro;
    }

    public List<ScenicNoticePreferentialPolicy> getPreferential_policy() {
        return preferential_policy;
    }

    public void setPreferential_policy(List<ScenicNoticePreferentialPolicy> preferential_policy) {
        this.preferential_policy = preferential_policy;
    }

    public List<ScenicNoticeIntro> getPreferential_policy_ext() {
        return preferential_policy_ext;
    }

    public void setPreferential_policy_ext(List<ScenicNoticeIntro> preferential_policy_ext) {
        this.preferential_policy_ext = preferential_policy_ext;
    }

    public List<ScenicNoticeFacility> getFacility() {
        return facility;
    }

    public void setFacility(List<ScenicNoticeFacility> facility) {
        this.facility = facility;
    }

    public String getOpen_time() {
        return open_time;
    }

    public void setOpen_time(String open_time) {
        this.open_time = open_time;
    }

    public List<ScenicNoticeIntro> getOpen_time_ext() {
        return open_time_ext;
    }

    public void setOpen_time_ext(List<ScenicNoticeIntro> open_time_ext) {
        this.open_time_ext = open_time_ext;
    }

    public Integer getPet() {
        return pet;
    }

    public void setPet(Integer pet) {
        this.pet = pet;
    }

    public List<ScenicNoticeIntro> getScenic_intro() {
        return scenic_intro;
    }

    public void setScenic_intro(List<ScenicNoticeIntro> scenic_intro) {
        this.scenic_intro = scenic_intro;
    }

    public List<ScenicNoticeIntro> getTraffic() {
        return traffic;
    }

    public void setTraffic(List<ScenicNoticeIntro> traffic) {
        this.traffic = traffic;
    }
}
